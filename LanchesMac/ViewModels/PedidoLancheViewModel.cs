﻿using LanchesMac.Migrations;
using LanchesMac.Models;

namespace LanchesMac.ViewModels
{
    public class PedidoLancheViewModel
    {
        public Pedido Pedido { get; set; }
        public IEnumerable <Models.PedidoDetalhe> PedidoDetalhe { get; set; }
    }
}
